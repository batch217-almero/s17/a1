//console.log("activity test");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserData(){
		let userFullName = prompt("Enter your fullname: ");
		let userAge  = prompt("Enter your age: ");
		let userLocation  = prompt("Enter your location: ");
		alert("Thank  you for your input!");


		console.log("Hello, " + userFullName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userLocation);
	}

	getUserData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavoriteBandsArtist () {
		console.log("1. 5 Seconds of Summer");
		console.log("2. The Carpenters");
		console.log("3. The Chainsmokers");
		console.log("4. Rex Orange County");
		console.log("5. Gryffin");
	}

	displayFavoriteBandsArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFavoriteMovies() {
		console.log("1. Interstellar\nRotten Tomatoes Rating: 86%");
		console.log("2. My Neighbor Totoro\nRotten Tomatoes Rating: 94%");
		console.log("3. Spirited Away\nRotten Tomatoes Rating: 96%");
		console.log("4. Flipped\nRotten Tomatoes Rating: 78%");
		console.log("5. The Sound of Music\nRotten Tomatoes Rating: 91%");
	}

	displayFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

//console.log(friend1);
//console.log(friend2);